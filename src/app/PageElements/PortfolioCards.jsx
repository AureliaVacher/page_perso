import React, { Component } from 'react'
//styles
import '../../styles/App.scss'
import planneo from '../../ressources/img/planneo.png'

class PortfolioCards extends Component {
    render() {
        return (
            <section className='portfolioContainer'>
                <div className='portfolioCards'>
                    <a href='https://planneo.netlify.com/' alt='Planneo' >
                        <img src={planneo} title='Planneo' target="_blank" alt='Planneo'/>
                    </a>
                    <h3>Planneo</h3>
                    <div className='portfolioCardsContent'>
                        <h6>WebApp for event management</h6>
                        <p>React.js, SAAS, PHP, MySQL, Firebase, API REST</p>
                        <p>Creation and development of responsive UI. <br/> Integration of node modules (Material UI, ReduxForm)</p>
                    </div>
                </div>
            </section>
        )
    }
}

export default PortfolioCards