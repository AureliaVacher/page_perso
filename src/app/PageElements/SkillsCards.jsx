import React, { Component } from 'react'
//styles
import '../../styles/App.scss'
import coding from '../../ressources/icon/coding.svg'
import education from '../../ressources/icon/education (1).svg'
import more from '../../ressources/icon/idea.svg'

class SkillsCards extends Component {
    render() {
        return (
            <section className='skillsContainer'>
                <div className='skillsCards'>
                    {/* <div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div> */}
                    <img src={coding} title='Web development' alt='Web development'/>
                    <h3>Full-Stack Developer</h3>
                    <div className='skillsCardsContent'>
                        <h6>Languages</h6>
                        <p>HTML, CSS, Sass, Javascript, Node.js, SQL, MongoDB</p>
                        <h6>Web tools</h6>
                        <p>Visual Studio Code, React.js, JQuery, Bootstrap, Github</p>
                    </div>
                </div>
                <div className='skillsCards'>
                    {/* <div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div> */}
                    <img src={education} title='Studies' alt='Studies'/>
                    <h3>Studies</h3>
                    <div className='skillsCardsContent'>
                        <p>
                            2018 - 2019: Full Stack development, Webschool Academy<br/>
                        </p>
                        <p>
                            2014 - 2016: Master's degree in project management, <br />
                            University of Blaise-Pascal
                        </p>
                    </div>
                </div>
                <div className='skillsCards'>
                    {/* <<div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div> */}
                    <img src={more} title='More' alt='More'/>
                    <h3>More</h3>
                    <div className='skillsCardsContent'>
                        <h6>Technical skills</h6>
                        <p>
                            Photoshop,
                            Illustrator,
                            Indesign
                        </p>
                        <h6>Languages</h6>
                        <p>
                            Hebrew: good, <br/>
                            English: very good, <br/>
                            French: mother tongue
                        </p>
                    </div>
                </div>
            </section>
        )
    }
}

export default SkillsCards